#!/bin/bash

until docker exec mysql_m sh -c 'export MYSQL_PWD=111; mysql -u root -e ";"'
do
    echo "Waiting for mysql_m database connection..."
    sleep 4
done

echo "Master is connected"

create_table='USE mydb; CREATE TABLE IF NOT EXISTS test(test_id INT NOT NULL AUTO_INCREMENT,data VARCHAR(100) NOT NULL,PRIMARY KEY ( test_id ));'
docker exec mysql_m sh -c "export MYSQL_PWD=111; mysql -u root -e '$create_table'"

for i in {1..5}
do
   echo "Insert $i record"
   insert_record='USE mydb; INSERT INTO test(data) VALUES("test data");'
   docker exec mysql_m sh -c "export MYSQL_PWD=111; mysql -u root -e '$insert_record'"

   sleep 1
done

until docker-compose exec mysql_s1 sh -c 'export MYSQL_PWD=111; mysql -u root -e ";"'
do
    echo "Waiting for slave_1 database connection..."
    sleep 4
done
echo "Slave 1 is connected"

read_data='USE mydb; SELECT * from test;'

echo "Read data from slave 1"
docker exec mysql_s1 sh -c "export MYSQL_PWD=111; mysql -u root -e '$read_data'"

until docker-compose exec mysql_s2 sh -c 'export MYSQL_PWD=111; mysql -u root -e ";"'
do
    echo "Waiting for slave_2 database connection..."
    sleep 4
done
echo "Slave 2 is connected"
echo "Read data from slave 2"
docker exec mysql_s2 sh -c "export MYSQL_PWD=111; mysql -u root -e '$read_data'"

# Remove column

echo "Remove column from slave 2"

remove_column='USE mydb; ALTER TABLE test DROP COLUMN data;'
docker exec mysql_s2 sh -c "export MYSQL_PWD=111; mysql -u root -e '$remove_column'"
docker exec mysql_s2 sh -c "export MYSQL_PWD=111; mysql -u root -e '$read_data'"

echo "Insert 2 records"

for i in {1..2}
do
   insert_record='USE mydb; INSERT INTO test(data) VALUES("test data with removed column");'
   docker exec mysql_m sh -c "export MYSQL_PWD=111; mysql -u root -e '$insert_record'"

   sleep 1
done
echo "Read data from slave 2 again"

docker exec mysql_s2 sh -c "export MYSQL_PWD=111; mysql -u root -e '$read_data'"


## Stopping slave 1

docker-compose stop mysql_s1

echo "Insert 2 records"

for i in {1..2}
do
   insert_record='USE mydb; INSERT INTO test(data) VALUES("test data with stopped slave");'
   docker exec mysql_m sh -c "export MYSQL_PWD=111; mysql -u root -e '$insert_record'"

   sleep 1
done

docker-compose start mysql_s1

until docker-compose exec mysql_s1 sh -c 'export MYSQL_PWD=111; mysql -u root -e ";"'
do
    echo "Waiting for slave_1 database connection..."
    sleep 4
done
echo "Slave 1 is connected"
echo "Read data from slave 1 again"

docker exec mysql_s1 sh -c "export MYSQL_PWD=111; mysql -u root -e '$read_data'"
